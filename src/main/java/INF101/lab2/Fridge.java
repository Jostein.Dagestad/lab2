package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    ArrayList<FridgeItem> fridgeItems = new ArrayList<>(); 
    

    @Override
    public int nItemsInFridge() {
        int countItems = 0;
        for (int i = 0; i < fridgeItems.size(); i++) {
            countItems += 1; 
        } 
        return countItems;
    }   
    

    @Override
    public int totalSize() {
        int maxItems = 20;
        return maxItems;
    }



    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgeItems.size()<20) {
            fridgeItems.add(item);
            return true;
        }
        return false;
    }


    @Override
    public void takeOut(FridgeItem item) {
      if(fridgeItems.contains(item)){
          fridgeItems.remove(item);
      }
      else{
        throw new NoSuchElementException();
    }
}


    @Override
    public void emptyFridge() {
        fridgeItems.clear();
    }



    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for(FridgeItem food : fridgeItems){
            if(food.hasExpired()){
                expiredItems.add(food);
            }
        }
            for(FridgeItem food : expiredItems) {
                fridgeItems.remove(food);
            }
      
        return expiredItems;
    }

    
}

